<?php

namespace App\vendor;

use App\Http\Services\Router\Router;
use App\Http\Services\Router\RouterDispatcher;

class App
{
    public static function run(): void
    {
        RouterDispatcher::enable(new Router());
    }
}