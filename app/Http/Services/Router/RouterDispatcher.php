<?php

namespace App\Http\Services\Router;
class RouterDispatcher
{
    public static function enable(Router $router): void
    {
        $uri = $_SERVER['REQUEST_URI'];
        $method = $_SERVER['REQUEST_METHOD'];
        $router->dispatch($method, $uri);
    }
}
