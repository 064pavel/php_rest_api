<?php

namespace App\Http\Services\Router;

class Router
{
    private static array $routes = [];

    public static function get(string $uri, array $controller): void
    {
        self::addRoute('GET', $uri, $controller);
    }

    public static function post(string $uri, array $controller): void
    {
        self::addRoute('POST', $uri, $controller);
    }

    public static function patch(string $uri, array $controller): void
    {
        self::addRoute('PATCH', $uri, $controller);
    }

    public static function delete(string $uri, array $controller): void
    {
        self::addRoute('DELETE', $uri, $controller);
    }

    private static function addRoute(string $method, string $uri, array $controller, ?int $id = null): void
    {
        $route = [
            'method' => $method,
            'uri' => $uri,
            'controller' => $controller,
            'id' => $id
        ];

        self::$routes[] = $route;
    }

    public static function dispatch(string $method, string $uri): void
    {
        $matchedRoute = null;

        foreach (self::$routes as $route) {
            if ($route['method'] === $method && self::isUriMatched($route['uri'], $uri)) {
                $matchedRoute = $route;
                break;
            }
        }

        if ($matchedRoute) {
            $controller = $matchedRoute['controller'][0];
            $action = $matchedRoute['controller'][1];

            if (class_exists($controller)) {
                $controllerInstance = new $controller();

                if (method_exists($controllerInstance, $action)) {
                    $requestData = self::getRequestData($method, $uri);

                    $controllerInstance->$action(...$requestData);

                    return;
                }
            }
        }

        http_response_code(404);
        echo '404 Not Found';
    }

    private static function isUriMatched(string $pattern, string $uri): bool
    {
        $pattern = preg_replace('/\{[\w]+\}/', '[\w]+', $pattern);
        $pattern = str_replace('/', '\/', $pattern);
        $pattern = '/^' . $pattern . '$/';

        return preg_match($pattern, $uri) === 1;
    }

    private static function getRequestData(string $method, string $uri): array
    {
        $requestData = [];

        if ($method === 'GET') {
            $requestData = [$_GET];
        } elseif ($method === 'POST' || $method === 'PATCH' || $method === 'DELETE') {
            $id = self::getIdFromUri($uri);
            $requestData = [self::parseRequestData(), $id];
        }

        return $requestData;
    }

    private static function parseRequestData(): array
    {
        $requestData = [];

        $contentType = $_SERVER['CONTENT_TYPE'] ?? '';
        if (str_contains($contentType, 'application/json')) {
            $requestData = json_decode(file_get_contents('php://input'), true);
        } else {
            parse_str(file_get_contents('php://input'), $requestData);
        }

        return $requestData;
    }

    private static function getIdFromUri(string $uri): ?int
    {
        $segments = explode('/', $uri);
        $id = end($segments);
        return is_numeric($id) ? (int)$id : null;
    }
}