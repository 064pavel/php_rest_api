<?php

namespace App\Http\Controllers;

use App\Http\Model\Customer;


class CustomerController
{
    public function index(): void
    {
        try {
            header('Content-Type: application/json');

            $customers = Customer::query()->select()->get();
            echo json_encode($customers);
        } catch (\Exception $e) {
            echo json_encode(['error' => $e->getMessage()]);
        }
    }

    public function show($id): void
    {
        try {
            $customer = Customer::query()->select()->where('id', '=', $id)->first();
            echo json_encode($customer);
        } catch (\Exception $e) {
            echo json_encode(['error' => $e->getMessage()]);
        }
    }

    public function store(): void
    {
        header('Content-Type: application/json');

        $requestData = json_decode(file_get_contents('php://input'), true);

        try {
            $customer = Customer::query()->create($requestData);
            echo json_encode(['success' => true, 'message' => 'Data saved successfully']);
        } catch (\Exception $e) {
            echo json_encode(['error' => $e->getMessage()]);
        }
    }


    public function update(): void
    {
        header('Content-Type: application/json');

        $requestData = json_decode(file_get_contents('php://input'), true);

        try {
            $customerId = $requestData['id'];
            unset($requestData['id']);

            $rowsAffected = Customer::query()->update($customerId, $requestData);

            if ($rowsAffected > 0) {
                echo json_encode(['success' => true, 'message' => 'Data updated successfully']);
            } else {
                echo json_encode(['error' => 'No data updated']);
            }
        } catch (\Exception $e) {
            echo json_encode(['error' => $e->getMessage()]);
        }
    }

    public function delete($id = null): void
    {
        header('Content-Type: application/json');

        if ($id === null) {
            echo json_encode(['error' => 'Invalid ID']);
        } else echo json_encode('id:'.$id);

        try {
                $customer = Customer::query()->delete($id);
                echo json_encode(['success' => true, 'message' => 'Data deleted successfully']);

        } catch (\Exception $e) {
            echo json_encode(['error' => $e->getMessage()]);
        }
    }


}