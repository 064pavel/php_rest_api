<?php

namespace App\Http\Model;

interface ModelInterface
{
    public function table(string $table): static;
    public function select(array $columns = ['*']): static;
    public function where(string $column, string $operator, string $value): static;
    public function orderBy(string $column, string $direction = 'ASC'): static;
    public function limit(int $count): static;
    public function get();
    public function first(): ?array;
    public function delete($id): int;
    public function create(array $data): bool;
    public static function update(int $id, array $data): int;
}