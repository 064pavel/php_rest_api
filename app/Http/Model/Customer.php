<?php

namespace App\Http\Model;

class Customer extends Model
{
    protected $parameters = [];

    public function __construct(string $table = 'customers')
    {
        parent::__construct($table);
        $this->parameters = [];

        static::setTableName($table);
    }
    public static function query(): Customer
    {
        return new Customer();
    }
}