<?php

namespace App\Http\Model;

use database\Database;
use PDO;

class Model implements ModelInterface
{
    protected static $tableName;

    protected $pdo;
    protected $table;
    protected $query;

    public function __construct(string $table = null)
    {
        $database = new Database();
        $this->pdo = $database->connect();
        $this->table = $table;
    }

    public static function setTableName(string $tableName)
    {
        static::$tableName = $tableName;
    }
    public function table(string $table): static
    {
        $this->table = $table;
        return $this;
    }

    public function select(array $columns = ['*']): static
    {
        if (empty($columns)) {
            $columns = ['*'];
        } else {
            $columns = implode(', ', $columns);
        }

        $this->query = "SELECT $columns FROM $this->table";
        return $this;
    }

    public function where(string $column, string $operator, $value): static
    {
        if (is_array($value)) {
            if (count($value) > 0) {
                $placeholders = implode(', ', array_fill(0, count($value), '?'));
                $this->query .= " WHERE $column $operator ($placeholders)";
                $this->parameters = array_merge($this->parameters, $value);
            }
        } else {
            $this->query .= " WHERE $column $operator ?";
            $this->parameters[] = $value;
        }

        return $this;
    }

    public function orderBy(string $column, string $direction = 'ASC'): static
    {
        $this->query .= "ORDER BY $column $direction";
        return $this;
    }

    public function limit(int $count): static
    {
        $this->query .= " LIMIT $count";
        return $this;
    }

    public function get()
    {
        if (empty($this->query)) {
            return [];
        }

        $stmt = $this->pdo->prepare($this->query);
        $stmt->execute($this->parameters);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function first(): ?array
    {
        $result = $this->get();
        return $result[0] ?? null;
    }

    public function delete($id): int
    {
        $this->query = "DELETE FROM $this->table WHERE id = ?";
        $this->parameters = [$id];

        $stmt = $this->pdo->prepare($this->query);
        $stmt->execute($this->parameters);

        return $stmt->rowCount();
    }

    public function create(array $data): bool
    {
        $columns = implode(', ', array_keys($data));
        $values = ':' . implode(', :', array_keys($data));
        $query = "INSERT INTO $this->table ($columns) VALUES ($values)";

        $stmt = $this->pdo->prepare($query);
        return $stmt->execute($data);
    }

    public static function update(int $id, array $data): int
    {
        $setClause = '';
        $params = [];

        foreach ($data as $key => $value) {
            $setClause .= "$key = :$key, ";
            $params[":$key"] = $value;
        }

        $setClause = rtrim($setClause, ', ');

        $query = "UPDATE " . static::$tableName . " SET $setClause WHERE id = :id";

        $params[':id'] = $id;

        $model = new static();

        $stmt = $model->pdo->prepare($query);
        $stmt->execute($params);

        return $stmt->rowCount();
    }
}