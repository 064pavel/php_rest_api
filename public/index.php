<?php

use App\vendor\App;

require_once '../vendor/autoload.php';
require_once '../routes/web.php';

App::run();
