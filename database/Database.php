<?php

namespace database;

use PDO;

class Database
{
    public function connect(): PDO
    {
        $host = 'mvc_db';
        $dbname = 'mvc';
        $username = 'user';
        $password = 'password';
        $port = 3306;

        try {
            $pdo = new PDO("mysql:host=$host;dbname=$dbname;port=$port;", $username, $password);
            return $pdo;
        } catch (\PDOException $exception) {
            echo $exception;
        }
    }
}