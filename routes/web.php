<?php

use App\Http\Controllers\CustomerController;
use App\Http\Services\Router\Router;

Router::get('/api/customers', [CustomerController::class, 'index']);
Router::patch('/api/customers', [CustomerController::class, 'update']);
Router::post('/api/customers', [CustomerController::class, 'store']);
Router::get('/api/customers/{id}', [CustomerController::class, 'show']);
Router::delete('/api/customers/{id}', [CustomerController::class, 'delete']);
